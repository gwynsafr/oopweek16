#include "application.h"
#include "datacollection.cpp"
#include "mainscreen.cpp"

Application::Application() = default;

void Application::run() {
  Mainscreen first_page;
  first_page.welcoming_message();
  first_page.available_options();
};