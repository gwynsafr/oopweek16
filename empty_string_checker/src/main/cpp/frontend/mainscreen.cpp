#include "mainscreen.h"

Mainscreen::Mainscreen() = default;

void Mainscreen::welcoming_message() {
  std::cout << "Welcome! This menu allows you to access Vba. Input digit representing available options to call "
               "that options\n";
};

void Mainscreen::available_options() {
  std::cout << "1 - Print a list of active contacts\n2 - About this program\n3 "
               "- Exit\n";
};

int Mainscreen::get_user_input() {
  int choosen_option = 0;
  std::cin >> choosen_option;
  if (choosen_option == 0) {
    std::cout << "Please, choose one of available options\n";
    available_options();
    choosen_option = get_user_input();
  }
  return choosen_option;
};