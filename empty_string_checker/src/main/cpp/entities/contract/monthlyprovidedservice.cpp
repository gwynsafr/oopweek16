#include "monthlyprovidedservice.h"

MonthlyProvidedService::MonthlyProvidedService() = default;
MonthlyProvidedService::MonthlyProvidedService(long id, std::string name, std::string description, double newServiceCostPerMonth) {
    ProvidedService::setId(id);
    ProvidedService::setName(name);
    ProvidedService::setDescription(description);
    this->ServiceCostPerMonth = newServiceCostPerMonth;
};

void Monthlyservice::recommendService() {
  std::cout << "This is a superb one. You should buy it.";
};