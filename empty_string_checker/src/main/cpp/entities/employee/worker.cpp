#include "worker.h"
#include "job.h"

Worker::Worker() = default;
Worker::Worker(long id, std::string name, Job job, std::string phone,
               int tier) {
  this->id = id;
  this->name = name;
  this->job = job;
  this->phone = phone;
  this->tier = tier;
};