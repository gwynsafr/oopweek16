#include "salariedjob.h"

SalariedJob::SalariedJob() = default;
SalariedJob::SalariedJob(long id, std::string name, double jobSalary) {
    Job::setId(id);
    this->name = name;
    this->jobSalary = jobSalary;
};

double SalariedJob::calculatePayment() {
    return this->jobSalary;
};