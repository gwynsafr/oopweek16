#include "workerservice.h"

std::list<Worker> WorkerService::searchByJob(std::list<Worker> workers, Job job) {
  std::list<Contract> requestedworkers;
  for (Worker worker : workers) {
    if (worker.getJob == job) {
      requestedworkers.push_back(worker);
    }
  }
  return requestedworkers;
}

std::list<Worker> WorkerService::getUnassignedWorkers(std::list<Worker> workers) {
  std::list<Contract> unassignedworkers;
  for (Worker worker : workers) {
    if (worker.getTier == 0) {
      unassignedworkers.push_back(worker);
    }
  }
  return unassignedworkers;
  }