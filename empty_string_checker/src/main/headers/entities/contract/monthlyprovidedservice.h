#ifndef UNTITLEDCLASSES_MonthlyProvidedService_H
#define UNTITLEDCLASSES_MonthlyProvidedService_H

#include <iostream>
#include <string>
#include <utility>

#include "service.h"

class MonthlyProvidedService : private Service {
private:
  double ServiceCostPerMonth = 0.0;
public:
  MonthlyProvidedService(int id, std::string name, std::string description, double newServiceCostPerMonth);

  MonthlyProvidedService();

  friend std::ostream &operator<<(std::ostream &os,
                                  const MonthlyProvidedService &service) {
    std::cout << service.getName() << " | " << service.ServiceCostPerMonth << " | "
              << service.getDescription() << std::endl;
  };

  const double &getServiceCostPerMonth() const {
    return this->ServiceCostPerMonth;
  };
  void setServiceCostPerMonth(const double &newServiceCostPerMonth) {
    this->ServiceCostPerMonth = newServiceCostPerMonth;
  };
  void recommendService();
};

#endif // UNTITLEDCLASSES_MonthlyProvidedService_H