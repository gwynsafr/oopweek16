#ifndef UNTITLEDCLASSES_PAYMENTCALCULATOR_H
#define UNTITLEDCLASSES_PAYMENTCALCULATOR_H

class PaymentCalculator {
public:
    virtual double calcultatePayment() = 0;
};

#endif // UNTITLEDCLASSES_PAYMENTCALCULATOR_H