#ifndef UNTITLEDCLASSES_ContractService_H
#define UNTITLEDCLASSES_ContractService_H

#include <iostream>
#include <string>
#include <utility>
#include <list>
#include <map>

#include "datacollection.h"

#include "contractsfilter.h"
#include "serviceusagecounter.h"

class ContractService: ActiveContactFilter, ServiceUsageCounter {
public:
  std::list<Contract> getActiveContracts(std::list<Contract> contracts);
  std::list<Contract> countServicesUsage(std::map<ProvidedService, int> popularservice)
};

#endif // UNTITLEDCLASSES_ContractService_H