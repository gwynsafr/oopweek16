#ifndef UNTITLEDCLASSES_WorkerService_H
#define UNTITLEDCLASSES_WorkerService_H

#include <iostream>
#include <string>
#include <utility>
#include <list>

#include "datacollection.h"
#include "byjobsearch.h"
#include "unassignedworkersfilter.h"

class WorkerService: ByJobSearch, UnassignedWorkersFilter {
public:
  std::list<Worker> searchByJob(std::list<Worker>);
  std::list<Worker> getUnassignedWorkers(std::list<Worker>);
};

#endif // UNTITLEDCLASSES_WorkerService_H