#ifndef UNTITLEDCLASSES_UNASSIGNEDWORKERSFILTER_H
#define UNTITLEDCLASSES_UNASSIGNEDWORKERSFILTER_H

#include <list>
#include <worker.h>

class UnassignedWorkersFilter {
public:
    virtual std::list<Worker> getUnassignedWorkers() = 0;
};

#endif // UNTITLEDCLASSES_UNASSIGNEDWORKERSFILTER_H