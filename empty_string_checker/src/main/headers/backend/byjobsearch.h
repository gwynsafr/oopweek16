#ifndef UNTITLEDCLASSES_BYJOBSEARCH_H
#define UNTITLEDCLASSES_BYJOBSEARCH_H

#include <list>
#include <worker.h>

class ByJobSearch {
public:
    virtual std::list<Worker> searchByJob() = 0;
};

#endif // UNTITLEDCLASSES_BYJOBSEARCH_H