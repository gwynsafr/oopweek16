#ifndef UNTITLEDCLASSES_SERVICEUSAGECOUNTER_H
#define UNTITLEDCLASSES_SERVICEUSAGECOUNTER_H

#include <map>
#include <providedservice.h>

class ServiceUsageCounter {
public:
    virtual std::map<ProvidedService, int> countServicesUsage() = 0;
};

#endif // UNTITLEDCLASSES_SERVICEUSAGECOUNTER_H
