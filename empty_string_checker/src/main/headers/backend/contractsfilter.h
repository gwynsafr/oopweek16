#ifndef UNTITLEDCLASSES_ACTIVECONTRACTFILTER_H
#define UNTITLEDCLASSES_ACTIVECONTRACTFILTER_H

#include <list>
#include <contract.h>

class ActiveContactFilter {
public:
    virtual std::list<Contract> getActiveContract() = 0;
};

#endif // UNTITLEDCLASSES_ACTIVECONTRACTFILTER_H