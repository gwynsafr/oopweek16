#ifndef UNTITLEDCLASSES_PRINTABLE_H
#define UNTITLEDCLASSES_PRINTABLE_H

class OptionMenu {
public:
    virtual void available_options() = 0;
};

#endif // UNTITLEDCLASSES_PRINTABLE_H
