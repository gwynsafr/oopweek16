#ifndef UNTITLEDCLASSES_USERINPUT_H
#define UNTITLEDCLASSES_USERINPUT_H

class UserInput {
public:
    virtual int get_user_input() = 0;
};

#endif // UNTITLEDCLASSES_USERINPUT_H
