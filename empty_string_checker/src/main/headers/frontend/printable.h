#ifndef UNTITLEDCLASSES_PRINTABLE_H
#define UNTITLEDCLASSES_PRINTABLE_H

class Printable {
public:
    virtual void print() = 0;
};

#endif // UNTITLEDCLASSES_PRINTABLE_H
