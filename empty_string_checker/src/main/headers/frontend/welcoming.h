#ifndef UNTITLEDCLASSES_WELCOMING_H
#define UNTITLEDCLASSES_WELCOMING_H

class Welcoming {
public:
    virtual void welcoming_message() = 0;
};

#endif // UNTITLEDCLASSES_WELCOMING_H
