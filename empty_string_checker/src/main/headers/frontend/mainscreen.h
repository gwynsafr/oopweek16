#ifndef UNTITLEDCLASSES_MAINSCREEN_H
#define UNTITLEDCLASSES_MAINSCREEN_H

#include "welcoming.h"
#include "userinput.h"
#include "optionmenu.h"

class Mainscreen: Welcoming, UserInput, OptionMenu {
public:
  Mainscreen();

  void welcoming_message();
  void available_options();
  int get_user_input();
};

#endif // UNTITLEDCLASSES_MAINSCREEN_H