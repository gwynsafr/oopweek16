#ifndef UNTITLEDCLASSES_SIMPLEREPORT_H
#define UNTITLEDCLASSES_SIMPLEREPORT_H

#include "printable.h"

class SimpleReport: Printable {
public:
    template<class T> void print() = 0;
};

#endif // UNTITLEDCLASSES_SIMPLEREPORT_H